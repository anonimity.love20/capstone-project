import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateNotificationPageRoutingModule } from './update-notification-routing.module';
import { NbCardModule, NbButtonModule, NbIconModule, NbCheckboxModule, NbInputModule, NbDatepickerModule, NbTooltipModule, NbUserModule, NbListModule } from '@nebular/theme';
import { UpdateNotificationPage } from './update-notification.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    NbInputModule,
    NbDatepickerModule,
    NbTooltipModule,
    NbUserModule,
    NbListModule,
    UpdateNotificationPageRoutingModule
  ],
  declarations: [UpdateNotificationPage]
})
export class UpdateNotificationPageModule {}
