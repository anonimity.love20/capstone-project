
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpdatePuvPageRoutingModule } from './update-puv-routing.module';
import { UpdatePuvPage } from './update-puv.page';
import { GoogleMapsModule } from '@angular/google-maps';

import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NbSelectModule, NbCardModule, NbButtonModule, NbIconModule, NbCheckboxModule, NbInputModule, NbDatepickerModule, NbTooltipModule, NbUserModule, NbListModule } from '@nebular/theme';

@NgModule({
  imports: [
    CommonModule,
    GoogleMapsModule,
    FormsModule,
    IonicModule,
    NbSelectModule,
    UpdatePuvPageRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    NbInputModule,
    NbDatepickerModule,
    NbTooltipModule,
    NbUserModule,
    NbListModule
  ],
  declarations: [UpdatePuvPage]
})
export class UpdatePuvPageModule { }

