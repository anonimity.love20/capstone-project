import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewPuvPage } from './view-puv.page';

const routes: Routes = [
  {
    path: '',
    component: ViewPuvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewPuvPageRoutingModule {}
