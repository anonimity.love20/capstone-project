import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewPuvPageRoutingModule } from './view-puv-routing.module';
import { GoogleMapsModule } from '@angular/google-maps';
import { ViewPuvPage } from './view-puv.page';
import { NbSelectModule, NbCardModule, NbButtonModule, NbIconModule, NbCheckboxModule, NbInputModule, NbDatepickerModule, NbTooltipModule } from '@nebular/theme';

@NgModule({
  imports: [
    NbSelectModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    NbInputModule,
    NbDatepickerModule,
    NbTooltipModule,
    CommonModule,
    GoogleMapsModule,
    FormsModule,
    IonicModule,
    ViewPuvPageRoutingModule
  ],
  declarations: [ViewPuvPage]
})
export class ViewPuvPageModule {}
