import { Component, OnInit, ViewChild } from "@angular/core";
import { GoogleMap, MapInfoWindow, MapPolyline } from "@angular/google-maps";
import { Events, NavController, ToastController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { VehicleService } from "../../services/vehicle.service";

@Component({
  selector: "app-view-puv",
  templateUrl: "./view-puv.page.html",
  styleUrls: ["./view-puv.page.scss"]
})
export class ViewPuvPage implements OnInit {
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap;
  @ViewChild(MapInfoWindow, { static: false }) infoWindow: MapInfoWindow;
  @ViewChild(MapPolyline, { static: false }) poly: MapPolyline;
  public marker_positions: google.maps.LatLngLiteral[] = [];
  public marker_options = { draggable: false, title: "" };
  public routes: Array<any> = [];

  constructor(
    private events: Events,
    private activatedRoute: ActivatedRoute,
    private vehicleService: VehicleService,
    private toast: ToastController
  ) {}

  ngOnInit() {
    this.events.publish("has_menu", true);
    this.events.publish("has_header", true);
    // this.init();
  }

  ngAfterViewInit() {
    this.init();
  }
  async init() {
    let id = this.activatedRoute.snapshot.params.id;
    let result: any = await this.vehicleService.GetById(id);
    let path = this.poly.getPath();
    let directionService = new google.maps.DirectionsService();
    if (!result.error && result.data) {
      let latLng = result.data.routes[2];
      this.marker_positions = result.data.routes;
      // let origin = new google.maps.LatLng(this.marker_positions[0]);
      // let destination = new google.maps.LatLng(
      //   this.marker_positions[this.marker_positions.length - 1]
      // );

      for (let i = 0; i < this.marker_positions.length - 1; i++) {
        if (i !== this.marker_positions.length - 1) {
          let origin = new google.maps.LatLng(this.marker_positions[i]);
          let destination = new google.maps.LatLng(
            this.marker_positions[i + 1]
          );

          directionService.route(
            {
              origin: origin,
              destination: destination,
              travelMode: google.maps.TravelMode.DRIVING
            },
            (result, status) => {
              if (status == google.maps.DirectionsStatus.OK) {
                for (
                  var j = 0, len = result.routes[0].overview_path.length;
                  j < len;
                  j++
                ) {
                  path.push(result.routes[0].overview_path[j]);
                }
              }
            }
          );
        }
      }
      console.log(path)
      // directionService.route(
      //   {
      //     origin: origin,
      //     destination: destination,
      //     travelMode: google.maps.TravelMode.DRIVING
      //   },
      //   (result, status) => {
      //     if (status == google.maps.DirectionsStatus.OK) {
      //       for (
      //         var i = 1; i < this.marker_positions.length - 2;
      //         i++
      //       ) {
      //         path.push(new google.maps.LatLng(
      //           this.marker_positions[i]
      //         ));
      //       }
      //     }
      //   }
      // );

      // this.marker_positions.forEach(item => {
      //   let latlng = new google.maps.LatLng(item);
      // if (path.getLength() === 0) {
      //   path.push(latlng);
      // } else {
      //   directionService.route({
      //     origin : path.getAt(path.getLength() -1),
      //     destination : latlng,
      //     travelMode : google.maps.TravelMode.DRIVING
      //   }, (result,status) => {
      //     if (status == google.maps.DirectionsStatus.OK) {
      //       console.log(result.routes)
      //       for (var i = 0, len = result.routes[0].overview_path.length;i < len;i++) {
      //         path.push(result.routes[0].overview_path[i]);
      //       }
      //     }
      //   });
      // }
      // });
      setTimeout(() => {
        this.initMapConfiguration(latLng);
      });
    } else {
      this.toastMessageError("Could not load vehicle");
    }
  }

  async initMapConfiguration(latLng) {
    this.map.options = {
      mapTypeControl: true,
      scaleControl: true,
      zoomControl: true,
      fullscreenControl: false,
      streetViewControl: false,
      zoom: 15
    };
    this.map.center = { ...latLng };
  }

  async toastMessageSuccess(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'success',
    });
    return toast.present();
  }
  async toastMessageError(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: "bottom",
      color: 'danger',
    });
    return toast.present();
  }
}
// this.marker_positions = result.data.routes;
//       let latLng;
//       for (var i=0 ; i < this.marker_positions.length ; i++){
//         latLng = {
//           lat: result.data.routes[i].lat,
//           lng: result.data.routes[i].lng
//         };
//         let latlng = new google.maps.LatLng(latLng)
//         let service = new google.maps.DirectionsService();
//         if (path.getLength() === 0) {
//           path.push(latlng);
//         } else {
//           service.route(
//             {
//               origin: path.getAt(path.getLength() - 1),
//               destination: latlng,
//               travelMode: google.maps.TravelMode.DRIVING
//             },
//             function(result, status) {
//               if (status == google.maps.DirectionsStatus.OK) {
//                 for (
//                   var i = 0, len = result.routes[0].overview_path.length;
//                   i < len;
//                   i++
//                 ) {
//                   path.push(result.routes[0].overview_path[i]);
//                   // this.paths = path.push(result.routes[0].overview_path[i]);
//                 }
//               }
//             }
//           );
//         }
//       }
