import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import ServerConfig from '../configs/server.config';

@Injectable({
  providedIn: "root"
})
export class HistoryService {
  constructor(private http: HttpClient, private storage: Storage) {}

  async Save(data) {
    let token = await this.storage.get("token")
    let history = await this.http.post(`${ServerConfig.API}/api/history/save?access_token=${token}`, data).toPromise()
    return history
  }
  async Listing() {
    let token = await this.storage.get("token")
    let histories = await this.http.get(`${ServerConfig.API}/api/history/listing?access_token=${token}`).toPromise()
    return histories
  }
}
