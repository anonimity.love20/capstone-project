
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, } from '@angular/fire/database';
import { Message } from '../pages/notification/notification';
import { User } from '../pages/notification/user';


@Injectable({
  providedIn: 'root'
})
export class MessageService {
 
  private msg_dbPath = '/message';
  private usr_dbPath = '/user';
  messageRef: AngularFireList<Message> = null;
  userRef: AngularFireList<User> = null;


 
  constructor(
    private db: AngularFireDatabase,
    ) {
    this.messageRef = this.db.list(this.msg_dbPath);
    this.userRef = this.db.list(this.usr_dbPath);
  }
 
  //send message to the User
  createUserMessage(message , id, date) {
    let _data = {
      message,
      date,
      is_clicked: false
    }
    return this.db.list('/user/'+id).push(_data)
  }

  //create message for the admin
  createAdminMessage(message,username,date) {
    let data = {
      message: message,
      created_at: date,
      status: 'Created',
      created_by: username,
    }
    this.messageRef.push(data);
    return data
  }

  //update specific message
  updateMessage(key: string, value: any): Promise<void> {
    return this.messageRef.update(key, value);
  }

  //update specific message for the user
  updateUserMessage(key: string, value: any): Promise<void> {
    return this.messageRef.update(key, value);
  }
 
  //get all messages
  getMessageList(): AngularFireList<Message> {
    return this.messageRef;
  }

  getUserList(): AngularFireList<User> {
    return this.userRef;
  }

  //message by id
  getMessageListByid(id) {
    return this.db.list('/message/'+id)
  }
  
}