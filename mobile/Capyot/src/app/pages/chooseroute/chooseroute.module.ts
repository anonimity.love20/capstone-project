import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseroutePageRoutingModule } from './chooseroute-routing.module';

import { ChooseroutePage } from './chooseroute.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseroutePageRoutingModule
  ],
  declarations: [ChooseroutePage]
})
export class ChooseroutePageModule {}
