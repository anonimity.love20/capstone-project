import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SendOtpPageRoutingModule } from './send-otp-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { SendOtpPage } from './send-otp.page';
import { CodeInputModule } from 'angular-code-input';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CodeInputModule,
    ComponentsModule,
    SendOtpPageRoutingModule
  ],
  declarations: [SendOtpPage]
})
export class SendOtpPageModule {}
