import { Component, OnInit } from "@angular/core";
import {
  NavController,
  LoadingController,
  ToastController
} from "@ionic/angular";
import { UsersService } from "../../services/users.service";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-send-otp",
  templateUrl: "./send-otp.page.html",
  styleUrls: ["./send-otp.page.scss"]
})
export class SendOtpPage implements OnInit {
  constructor(
    private nav: NavController,
    private loading: LoadingController,
    private usersService: UsersService,
    private authService: AuthService,
    private toast: ToastController
  ) {}

  public code: any;

  ngOnInit() {
    this.init();
  }

  async init() {
    
  }

  async verify() {
    let loading = await this.loading.create({
      message: "Loggin in please wait...",
      mode: "ios",
      duration: 1500
    });
    loading.present();
    let data = {
      code: this.code
    };
    let result: any = await this.authService.Verify(data);
    console.log(result);
    if (!result.error && result.data) {
      this.nav.navigateRoot("home");
    } else {
      this.toastMessage(result.message);
    }
  }

  async toastMessage(message) {
    let toast = await this.toast.create({
      message: message,
      duration: 2000,
      mode: "ios"
    });
    return toast.present();
  }
  async signup(){
    let loading = await this.loading.create({
      message: "Please wait...",
      duration: 2000,
      mode: "ios"
    });
    loading.present();
    this.nav.navigateRoot("signup");
  }
}
