import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewNotificationPageRoutingModule } from './view-notification-routing.module';
import { ComponentsModule } from '../../components/components.module';

import { ViewNotificationPage } from './view-notification.page';
// import { TimeAgoPipe } from 'time-ago-pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ViewNotificationPageRoutingModule
  ],
  declarations: [ViewNotificationPage,]
})
export class ViewNotificationPageModule {}
