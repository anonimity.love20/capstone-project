import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/services/message.service';
import { UsersService } from 'src/app/services/users.service';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-view-notification',
  templateUrl: './view-notification.page.html',
  styleUrls: ['./view-notification.page.scss'],
})
export class ViewNotificationPage implements OnInit {
  
  notification = [];
  id: any
  constructor(
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService,
    private usersService: UsersService,
  ) { }

  ngOnInit() {
    this.getUsersList();
    this.init()
  }
  
  async init(){
    this.id = this.activatedRoute.snapshot.params.id;
  }

   //get specific user and specific messages
   getUserListById(id) {
    let result: Array<any> =[]
    this.messageService
      .getUserListByid(id)
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key,...c.payload.val() }))
        )
      )
      .subscribe(user_messages => {
        //loop user messages using its key
        for (let index = 0; index < user_messages.length - 1; index++) {
          const element = user_messages[index];
          if(this.id === element.key){
            this.notification.push(user_messages[index])
          }
        }
      });
  }


  //get all users
 async getUsersList() {
    let result: any = await this.usersService.GetCurrent();
    let phone_number = result.data.owner.phone_number
    this.messageService
      .getUserList()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(users => {
        for (let index = 0; index < users.length; index++) {
          const element = users[index];
          if(element.phone_number != phone_number){
          }else {
            this.getUserListById(users[index].key)
          }
        }
      });
  }
}
