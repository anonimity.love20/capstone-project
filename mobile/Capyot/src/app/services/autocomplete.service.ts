import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class AutocompleteService {
  constructor(private http: HttpClient) {}

  async SearchAutoComplete(place: string) {
    let result = await this.http
      .get(
        `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${place}&types=establishment&location=10.3790718,123.7062059&radius=500&key=${environment.GOOGLE_MAPS_KEY}`, {}
      )
      .toPromise();
    console.log(result)
    return result;
  }
}
