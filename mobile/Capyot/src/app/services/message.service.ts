
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Message } from '../pages/notification/notification';
import { User } from '../pages/signup/user';


@Injectable({
  providedIn: 'root'
})
export class MessageService {
 
  private msg_dbPath = '/message';
  private usr_dbPath = '/user';

 
  messageRef: AngularFireList<Message> = null;
  userRef: AngularFireList<User> = null;
 
  constructor(
    private db: AngularFireDatabase,
    ) {
    this.messageRef = this.db.list(this.msg_dbPath);
    this.userRef = this.db.list(this.usr_dbPath);

  }
 
  //send user to the firebase
  createUser(phone_number) {
    let data = {
      phone_number: phone_number,
    }
    return this.userRef.push(data);
  }
 
  getMessageList(): AngularFireList<Message> {
    return this.messageRef;
  }

  //get all users
  getUserList(): AngularFireList<User> {
    return this.userRef;
  }

  //get specific user by id
  getUserListByid(id): AngularFireList<User> {
    return this.db.list('/user/'+id)
  }

  //update specific message for the user to change the is_clicked
  updateUserMessage(key: string, userId, message) {
    let value = {
      message,
      is_clicked: true
    }
    this.db.list('/user/'+userId).update(key, value)
  }


}