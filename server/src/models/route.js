import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schema = new Schema({
    landmark: String,
    lat: Number,
    lng: Number
})

module.exports = mongoose.model('Route', schema);