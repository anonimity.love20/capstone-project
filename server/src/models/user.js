import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const schema = new Schema({
    password: String,
    phone_number: String,
    username: String,
    url: String
});

module.exports = mongoose.model('User', schema);