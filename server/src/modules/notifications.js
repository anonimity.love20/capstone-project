import Notification from '../models/notification'

export default class Notifications {
    static async Save(data) {
        if (data._id) {
            let _id = data._id
            delete data._id
            return Notification.update({
                _id: _id
            }, {
                    $set: data
                })
        } else {
            let notification = new Notification(data)
            return await notification.save()
        }
    }

    static async NotificationsListing() {
        let notifications = await Notification.find({})
        if (notifications.length > 0) {
            return notifications
        } else {
            return null
        }
    }

    static async GetNotificationById(id) {
        return await Notification.find({
            _id: id
        })
    }
}