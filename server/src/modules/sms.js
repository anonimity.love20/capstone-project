import Nexmo from 'nexmo'
import { Nexmo_config } from '../config.json'


export default class Sms {

    static async Send(phone_number, message) {
        let nexmo = new Nexmo({
            apiKey: Nexmo_config.apiKey,
            apiSecret: Nexmo_config.apiSecret
        })
        let from = "Capyot Inc."
        nexmo.message.sendSms(from, phone_number, message, (err, response) => {
            console.log(phone_number)
            console.log(message)
            if (!err && response && response.messages && response.messages.length > 0) {
                let message = response.messages[0]
                console.log("message from nexmo, ", message)
                if (message.status == 0) {
                    return message
                } else {
                    return null
                }
            } else {
                return null
            }
        })
    }
}