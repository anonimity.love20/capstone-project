import Token from '../models/token';

export default class Tokens {

    static async GetByToken(token) {
        // return await Token.findOne({
        //     token: token
        // }).populate("owner");
        
        let _token = await Token.findOne({ token: token }).populate('owner')
        return _token
    }

    static async GetCodeByOwner(id) {
        let token = await Token.findOne({ owner: id })
        return token.code
    }

    static async Save(data) {
        if (data._id) {
            let id = data._id;
            delete data._id;
            return await Token.update({
                _id: id
            }, {
                    $set: data
                });
        } else {
            let token = new Token(data);
            return await token.save();
        }
    }
}