import Vehicles from '../modules/vehicles'

export const VehicleRoutes = [
    {
        method: 'POST',
        path: '/api/vehicle/save',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let data = {
                _id: req.payload.id,
                type: req.payload.type,
                code: req.payload.code,
                routes: req.payload.routes,
                update_status: req.payload.update_status
            }
            let vehicle = await Vehicles.Save(data)
            if (vehicle) {
                return {
                    error: 0,
                    data: vehicle,
                    message: 'Vehicle is successfully saved'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Error while saving vehicle'
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/vehicle/listing',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let vehicles = await Vehicles.VehiclesListing()
            if (vehicles) {
                return {
                    error: 0,
                    data: vehicles,
                    message: 'Fetching vehicles successful'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Fetching vehicles failed'
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/vehicle/edit/save',
        config: {
            auth: 'simple'
        },
        handler: async (req, res) => {
            let data = {
                id: req.payload.id,
                type: req.payload.type,
                code: req.payload.code,
                routes: [],
                update_status: req.payload.update_statuss
            }
            let vehicle = await Vehicles.Save(data)
            if (vehicle) {
                return {
                    error: 0,
                    data: vehicle,
                    message: 'Vehicle is successfully edited'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Error while editing vehicle'
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/vehicle/get/{id}',
        config: {
            auth: "simple"
        },
        handler: async (req, res) => {
            let vehicle = await Vehicles.GetById(req.params.id)
            if (vehicle) {
                return {
                    error: 0,
                    data: vehicle,
                    message: 'Successfully get the vehicle'
                }
            } else {
                return {
                    error: 1,
                    data: null,
                    message: 'Could not get vehicle'
                }
            }
        }
    }
]